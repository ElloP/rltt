﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Rltt.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="server">
    <asp:TextBox runat="server" ID="NameField"></asp:TextBox>
    <asp:Button runat="server" ID="AddPlayer" Text="Add Player" onClick="addPlayer"/>
    <p class="SystemText"><asp:Label runat="server" ID="NewPlayerMessage"></asp:Label></p>
    <asp:Repeater runat="server" ID="AddedPlayerRep">
        <ItemTemplate>
            <p class="SystemText">Name: <asp:Literal runat="server" ID="NewPlayerName" Text='<%# Eval("Name") %>'></asp:Literal></p>
            <p class="SystemText">Rating: <asp:Literal runat="server" ID="NewPlayerRating" Text='<%# Eval("Rating") %>'></asp:Literal></p>
            <p class="SystemText">Matches: <asp:Literal runat="server" ID="NewPlayerMatches" Text='<%# Eval("Matches") %>'></asp:Literal></p>
            <p class="SystemText">Wins: <asp:Literal runat="server" ID="NewPlayerWins" Text='<%# Eval("Wins") %>'></asp:Literal></p>
            <p class="SystemText">Losses: <asp:Literal runat="server" ID="NewPlayerLosses" Text='<%# Eval("Losses") %>'></asp:Literal></p>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
