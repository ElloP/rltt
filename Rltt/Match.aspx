﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd.Master" AutoEventWireup="true" CodeBehind="Match.aspx.cs" Inherits="Rltt.Match" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="server">
    <!-- Player list repeater -->
    <asp:Repeater runat="server" ID="PlayersRep" OnItemCommand="PlayersRep_ItemCommand">
        <HeaderTemplate>
            <asp:Label runat="server"><h3>Player list</h3></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <div class="mPlayers"><asp:Literal runat="server" ID="PlayerName" Text='<%# Eval("Name") %>'></asp:Literal><asp:CheckBox CssClass="PlayerCb" runat="server" ID="SelectPlayer" /></div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Player list bottom area -->
    <asp:Button runat="server" ID="AddToPool" Text="Add players" OnClick="AddToPool_Click"/>
    <asp:Label ID="Label1" runat="server"></asp:Label>
    <br />
    <br />

    <!--Active players repeater -->
    <asp:Repeater runat="server" ID="ActivePlayersRep">
        <HeaderTemplate>
            <asp:Label runat="server"><h3>Active players</h3></asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
            <div class="aPlayers"><asp:Literal ID="PlayerName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal><asp:CheckBox CssClass="PlayerCb" runat="server" ID="DeSelectPlayer" /></div>
            <asp:HiddenField ID="HiddenPlayer" runat="server" Value='<%# Eval("Id") %>' />
        </ItemTemplate>
    </asp:Repeater>

    <!--Bottom area-->
    <asp:Button runat="server" Text="Deselect" ID="DeSelectButton" OnClick="DeSelectButton_Click" />
    <asp:Button runat="server" Text="Play" ID="Play" OnClick="Play_Click"/>
    <asp:Literal runat="server" Text="Team size:"></asp:Literal>
        <asp:DropDownList runat="server" ID="TeamSize">
        <asp:ListItem runat="server" Text="1" Value="1"></asp:ListItem>
        <asp:ListItem runat="server" Text="2" Value="2"></asp:ListItem>
        <asp:ListItem runat="server" Text="3" Value="3"></asp:ListItem>
        <asp:ListItem runat="server" Text="4" Value="4"></asp:ListItem>
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label runat="server" CssClass="SystemText" ID="ErrorMsg"></asp:Label>
</asp:Content>