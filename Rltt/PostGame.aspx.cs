﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rltt
{
    public partial class PostGame : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string winner = Request.QueryString["Team"];
            Winner.Text = winner;
        }
    }
}