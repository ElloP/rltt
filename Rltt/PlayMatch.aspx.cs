﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rltt
{
    public partial class PlayMatch : System.Web.UI.Page
    {
        private List<Player> players = new List<Player>(); //list of all players
        private static MatchClass match; //ugly fix for postback bug that i dont wanna fix for real
        private int winningTeam;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                using(var db = new rlttDBEntities2())
                {
                    string strId = Request.QueryString["Id"]; //adds them to string
                    int[] id = Utils<String>.getInts(strId); //converts them to ints
                    foreach(int i in id) //selects all players and adds them to player list
                    {
                        var playerList = from getPlayer in db.tbl_players
                                         where getPlayer.Id == i
                                         select getPlayer;

                        foreach (var p in playerList)
                        {
                            players.Add(new Player(p.Name, p.Id, p.Rating, p.Matches, p.Wins, p.Losses));
                        }
                    }
                }
                match = new MatchClass(players);
                Team1.DataSource = match.Team1;
                Team1.DataBind();
                Team2.DataSource = match.Team2;   
                Team2.DataBind();
            }
        }

        protected void Radio_Click(object sender, EventArgs e)
        {
            if (Team1win.Checked)
            {
                winningTeam = 1;
            }
            else if (Team2win.Checked)
            {
                winningTeam = 2;
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {

            if (winningTeam == 1) // if winning team is 1 raise their rating and lower team 2
            {
                using (var db = new rlttDBEntities2()) //open db
                {
                    foreach (Player p in match.Team1) //raise each players rating in team 1
                    {
                        var dbPlayer = (from dbp in db.tbl_players
                                                where dbp.Id == p.id
                                                select dbp).First();
                        dbPlayer.Rating = p.raiseRating((int) match.averageRating(match.Team2), Player.MaxGain);
                        dbPlayer.Matches++;
                        dbPlayer.Wins++;
                    }

                    foreach (Player p in match.Team2)
                    {
                        var dbPlayer = (from dbp in db.tbl_players
                                        where dbp.Id == p.id
                                        select dbp).First();
                        dbPlayer.Rating = p.lowerRating((int)match.averageRating(match.Team1), Player.MaxGain);
                        dbPlayer.Matches++;
                        dbPlayer.Losses++;
                    }
                    db.SaveChanges();
                }
            }

            else if (winningTeam == 2) // if winning team is 2 raise their rating and lower team 1
            {
                using (var db = new rlttDBEntities2()) //open db
                {
                    foreach (Player p in match.Team2) //raise each players rating in team 2
                    {
                        var dbPlayer = (from dbp in db.tbl_players
                                        where dbp.Id == p.id
                                        select dbp).First();
                        dbPlayer.Rating = p.raiseRating((int)match.averageRating(match.Team1), Player.MaxGain);
                        dbPlayer.Matches++;
                        dbPlayer.Wins++;
                    }

                    foreach (Player p in match.Team1)
                    {
                        var dbPlayer = (from dbp in db.tbl_players
                                        where dbp.Id == p.id
                                        select dbp).First();
                        dbPlayer.Rating = p.lowerRating((int)match.averageRating(match.Team2), Player.MaxGain);
                        dbPlayer.Matches++;
                        dbPlayer.Losses++;
                    }
                    db.SaveChanges();
                }
            }
            Response.Redirect("PostGame.aspx?Team=" + winningTeam);
        }
    }
}