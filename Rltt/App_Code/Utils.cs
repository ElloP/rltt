﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Rltt
{
    abstract public class Utils <T>
    {
        public static Random rand = new Random(); //random object initialization
        public static int[] getInts(string s) // converts a querystring of ints separated by commas into an array of ints
        {
            string[] str_id = s.Split(',');
            int[] ints = new int[str_id.Length];
            for(int i = 0; i < str_id.Length; i++)
            {
                ints[i] = Convert.ToInt32(str_id[i]);
            }
            return ints;
        }
        
        public static List<T> GetRandomList(List<T> l)
        {
            for (int i = l.Count - 1; i > 0; i--) // simple implementation of fisher yates on a generic list
            {
                int j = rand.Next(i + 1);
                T temp = l[i];
                l[i] = l[j];
                l[j] = temp;
            }
            return l;
        }

        public static int matchmakingRange(int i) // different matchmaking ranges where i is number of tries to match teams together and the return value is the allowed difference of rating between the teams
        {
            if (i < 10)
            {
                return 10;
            }
            else if (i < 100)
            {
                return 100;
            }
            else
            {
                return i;
            }
        }
    }
}