﻿using System;
using System.Collections.Generic;

namespace Rltt
{
    public class MatchClass
    {
        public List<Player> Team1;
        public List<Player> Team2; 

        public MatchClass(List<Player> players)
        {
            List<Player> tempteam1 = new List<Player>();
            List<Player> tempteam2 = new List<Player>();

            for (int i = 0;
                (Math.Abs(averageRating(tempteam1) - averageRating(tempteam2))) > Utils<int>.matchmakingRange(i) || i == 0; //if difference of rating between two teams is larger than the method return keep matching new teams
                i++)
            {
                List<Player> randPlayers = Utils<Player>.GetRandomList(players); //gets a list of random ordered players and puts every other in every other team
                tempteam1 = new List<Player>();
                tempteam2 = new List<Player>();
                int c = 1;
                foreach (Player p in randPlayers)
                {
                    if (c % 2 == 0)
                    {
                        tempteam1.Add(p);
                    }
                    else
                    {
                        tempteam2.Add(p);
                    }
                    c++;
                }
            }
            Team1 = tempteam1;
            Team2 = tempteam2;
        }

        public double averageRating(List<Player> t) //gets average rating of a team
        {
            int size = t.Count != 0 ? t.Count : 1;
            int tRating = 0;
            foreach (Player p in t)
            {
                tRating += p.rating;
            }
            return tRating/(double)size;
        }


    }
}