﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace Rltt
{
    public class Player
    {
        public const int MaxGain = 32;
        public int id { get; private set; } //standard member variables for Player class, doesn't need more explanation
        public string name { get; private set; } 
        public int rating { get; set; }
        public int matches { get; set; }
        public int wins { get; set; }
        public int losses { get; set; }
     
        public Player (string name, int id, int rating, int matches, int wins, int losses) //public constructor for Player class
        {
            this.name = name;
            this.id = id;
            this.rating = rating;
            this.matches = matches;
            this.wins = wins;
            this.losses = losses;
        }
        /*
        public Player(string name) //public constructor for Player class
        {
            this.name = name;
            this.rating = 1500;
            this.matches = 0;
            this.wins = 0;
            this.losses = 0;
        }
        */
        public Player(string name, int id)
        {
            this.name = name;
            this.id = id;
        }

        public int raiseRating(int oRating, int maxGain)
        {
            double expectedScore = 1/(1 + Math.Pow(10, ((oRating - this.rating)/400)));
            int newRating = (int) (this.rating + maxGain * expectedScore);
            return newRating;
        }

        public int lowerRating(int oRating, int maxGain)
        {
            double expectedScore = 1/(1 + Math.Pow(10, ((oRating - this.rating)/400)));
            int newRating = (int) (this.rating - maxGain*expectedScore);
            return newRating;
        }

        public static void addPlayerToDB(string name) // method to add a new player to db with standard stats
        {      
            using(rlttDBEntities2 myEntity = new rlttDBEntities2())
            {
                tbl_players player = new tbl_players();
                player.Name = name;
                player.Rating = 1200;
                myEntity.tbl_players.Add(player);
                myEntity.SaveChanges();
            }
        }
    }
}