﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rltt
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void addPlayer(object sender, EventArgs e)
        {
            Player.addPlayerToDB(NameField.Text);
            using(var db = new rlttDBEntities2())
            {
                var player = from p in db.tbl_players
                             where p.Name == NameField.Text
                             select p;

                AddedPlayerRep.DataSource = player.ToList();
                AddedPlayerRep.DataBind();

                NewPlayerMessage.Text = "New Player Added!";
            }
        }
    }
}