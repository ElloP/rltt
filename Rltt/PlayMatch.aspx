﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontEnd.Master" AutoEventWireup="true" CodeBehind="PlayMatch.aspx.cs" Inherits="Rltt.PlayMatch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" runat="server">
    <br/>
    <div class="match">
        <div class="radio1box">
            <asp:Label runat="server" Text="Winner"></asp:Label>
            <br/>
            <asp:RadioButton runat="server" CssClass="t1Radio" ID="Team1win" Text="1" GroupName="Match1" OnCheckedChanged="Radio_Click"/>
        </div>
        <div class="team1">
            <asp:Repeater runat="server" ID="Team1">
                <ItemTemplate>
                    <asp:Literal runat="server" ID="PlayerName" Text='<%# Eval("name") %>'></asp:Literal><br/>
                </ItemTemplate>
            </asp:Repeater>
        </div>

        <h3 style="float:left;margin-top:10px;width:31%;">VS</h3>

        <div class="team2">
            <asp:Repeater runat="server" ID="Team2">
                <ItemTemplate>
                    <asp:Literal runat="server" ID="PlayerName" Text='<%# Eval("name") %>'></asp:Literal><br/>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="radio2box">
            <asp:Label runat="server" Text="Winner"></asp:Label>
            <br/>
            <asp:RadioButton runat="server" CssClass="t2Radio" ID="Team2win" Text="2" GroupName="Match1" OnCheckedChanged="Radio_Click"/>
        </div>
    </div>
    <br/>
    <asp:Button runat="server" ID="Submit" OnClick="Submit_Click" Text="Submit Match"/>
    <asp:Label runat="server" CssClass="SystemText" ID="ErrorMsg"></asp:Label>
</asp:Content>
