﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rltt
{
    public partial class Match : System.Web.UI.Page
    {
        //HiddenField hiddenPlayer = (HiddenField)i.FindControl("HiddenPlayer"); //2. hiddenfields name hiddenplayer
        private List<Player> players = new List<Player>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { //if not postback, select players from database and add them to the list "deselectedplayers" and bind to playersrep
                using (var db = new rlttDBEntities2())
                {
                    var playerList = from p in db.tbl_players
                                     orderby p.Name
                                     select new { p.Id, p.Name };

                    foreach (var p in playerList)
                    {
                        players.Add(new Player(p.Name, p.Id));
                    }

                    PlayersRep.DataSource = players;
                    PlayersRep.DataBind();
                    ActivePlayersRep.DataSource = players;
                    ActivePlayersRep.DataBind();

                    foreach (RepeaterItem i in ActivePlayersRep.Items)
                    {
                        i.Visible = false;
                    }
                }
            }
        }
        protected void PlayersRep_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }

        protected void AddToPool_Click(object sender, EventArgs e) // Adds the checked players to a list and binds them to the ActivePlayersRep repeater
        {
            foreach (RepeaterItem i in PlayersRep.Items)
            {
                CheckBox cb = (CheckBox)i.FindControl("SelectPlayer"); //sets variables to 1. checkboxes name selectplayer
                Literal playerName = (Literal)i.FindControl("PlayerName"); //2. literals name playername

                if (cb.Checked) //if checkbox is checked
                {
                    cb.Checked = false; //uncheck checkbox
                    i.Visible = false; //hide item

                    foreach (RepeaterItem i2 in ActivePlayersRep.Items) 
                    {
                        Literal activePlayerName = (Literal)i2.FindControl("PlayerName");
                        if (playerName.Text.Equals(activePlayerName.Text))
                        {
                            i2.Visible = true; //if item in second list is same as first show it
                        }
                    }
                }
            }
        }

        protected void DeSelectButton_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem i in ActivePlayersRep.Items)
            {
                CheckBox cb = (CheckBox)i.FindControl("DeSelectPlayer");   //sets variables to 1. checkboxes name selectplayer
                Literal activePlayerName = (Literal)i.FindControl("PlayerName"); //2. literals name playername

                if (cb.Checked)
                {
                    cb.Checked = false;
                    i.Visible = false;

                    foreach (RepeaterItem i2 in PlayersRep.Items)
                    {
                        Literal playerName = (Literal)i2.FindControl("PlayerName"); //3. literals name playername
                        if (activePlayerName.Text.Equals(playerName.Text))
                        {
                            i2.Visible = true;
                        }
                    }
                }
            }
        }

        protected void Play_Click(object sender, EventArgs e)
        {
            string qs = "";
            int pCount = 0;
            foreach(RepeaterItem i in ActivePlayersRep.Items)
            {
                if(i.Visible)
                {
                    pCount++; //counts players

                    HiddenField hiddenPlayer = (HiddenField)i.FindControl("HiddenPlayer");
                    if(qs.Equals(""))
                    {
                        qs += "Id=" + hiddenPlayer.Value;
                    }
                    else
                    {
                        qs += "&Id=" + hiddenPlayer.Value;
                    }
                }
            }

            if (pCount % Convert.ToInt32(TeamSize.SelectedValue) == 0 && pCount > Convert.ToInt32(TeamSize.SelectedValue) && (pCount / 2) == Convert.ToInt32(TeamSize.SelectedValue) && pCount % 2 == 0)
            {
                qs += "&ts=" + TeamSize.SelectedValue;
                Response.Redirect("PlayMatch.aspx?" + qs);
            }
            else
            {
                ErrorMsg.Text = "Number of players and teamsize much match. Currently only matches, not tournaments";
            }
        }
    }
}